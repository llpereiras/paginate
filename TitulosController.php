<?php
App::uses('AppController', 'Controller');

/**
 * Titulos Controller
 *
 * @property Titulo $Titulo
 */
class TitulosController extends AppController {

/**
 * index method
 *
 * @return void
 */
 
 public $paginate = array(
  	'limit' => 50
	);
	
	public function totais() {
	
		//  Traz o Model de usuario para listar os usuarios no select de Responsavel pela baixa
		$this->loadModel('Users');
		$this->set('users', $this->Users->find('all', 
							array(
							'fields' => array('Users.id','Users.username')
							),
							array(
							'order' => array('Users.username' => 'ASC')
							)
				)
		);
			
		$dados = $this->request->data;
			
		
		/*************************************************************************************/
		$conditions = "";
		$select_consulta = "";
		$text_consulta = "";
		$modulo = "";
		$tipo = "";
		$user_baixa = "";
		$dt_quitacao = "";
		$dtini = "";
		$dtfim = "";
		
		if ($_SERVER['REQUEST_METHOD'] == "POST"){
			if (isset($dados["tipo"]) != "" ) {
			 $tipo = $dados["tipo"];
			}
			
			if (isset($dados["select_consulta"])){
			$select_consulta= $dados["select_consulta"];
			}
			
			if (isset($dados["text_consulta"])){
			$text_consulta= $dados["text_consulta"];
			}
			
			if (isset($dados["dt_quitacao"])){
			$dt_quitacao = $dados["dt_quitacao"];
			}
					
			if (isset($dados["user_baixa"])){
			$user_baixa = $dados["user_baixa"];
			}

			if (isset($dados["dtini"])){
			$dtini = $dados["dtini"];
			}
			
			if (isset($dados["dtfim"])){
			$dtfim = $dados["dtfim"];
			}
		}
		
		
		if ($_SERVER['REQUEST_METHOD'] == "GET"){
			$urli = explode("/",$_SERVER['REQUEST_URI']);
			///////////////////print_r($urli);
			for ($i=0; $i< count($urli); $i++){
				$urlX = explode(":",$urli[$i] );
				if (count($urlX) > 1 ){
				
					 if ($urlX[0] == "tipo"){ 
						$tipo = str_replace ("_"," ",$urlX[1]);
					}
						
					 if ($urlX[0] == "select_consulta"){ 
						$select_consulta = $urlX[1];
					}
						
					if ($urlX[0] == "text_consulta"){ 
						$text_consulta = $urlX[1];
					}
					
					if ($urlX[0] == "dt_quitacao"){
					$dt_quitacao = $urlX[1];
					}
							
					if ($urlX[0] == "user_baixa"){
					$user_baixa = $urlX[1];
					}

					if ($urlX[0] == "dtini"){
					$dtini = $urlX[1];
					}
					
					if ($urlX[0] == "dtfim"){
					$dtfim = $urlX[1];
					}
					
			 
				}
			}
		}
		/*****************************************************************************************************/
					
		if ($tipo == "" && count($dados) == 0) {
			$options = array(
						'fields' => array('*'),
						'conditions' => array('1' => '0 ')
					);
			$this->paginate = $options;			
			$this->set('titulos', $this->paginate());
			$this->render();
		
		}else{
			$conditions = "";
			
			if($tipo == "aberto"){
				$conditions = array(
								'OR' => array(
											  array('Titulo.data_baixa' => null),
											  array('Titulo.data_baixa' => '')
										)
								);
			}else{
				if($tipo == "quitados"){
				$conditions = array(
					'AND'=> array(
						array('Titulo.data_baixa != ' => ''),
					)
				);
				}
			}
			
			if ($select_consulta != "" && !empty($select_consulta)) {
				$conditions['AND'][] = array('Titulo.' . $select_consulta  . ' LIKE' => '%' . $text_consulta . '%');
			}
			
			if (isset($user_baixa) && !empty($user_baixa)) {
				$conditions['AND'][] = array('Titulo.user_id_baixa ' => $user_baixa);
			}
			
			if (isset($dt_quitacao ) && !empty($dt_quitacao)) {
				$conditions['AND'][] = array('Titulo.data_baixa ' => $dt_quitacao );
			}
			
			/////if($dados["totais"] == "periodo"){
				if ( (isset($dtini) && !empty($dtini)) && (isset($dtfim) && !empty($dtfim)) ) {
				
					$conditions['AND'][] = array(
													 array(
														 'CONCAT(substring(`Titulo`.`vencimento`,7,4) , substring(`Titulo`.`vencimento`,4,2) , substring(`Titulo`.`vencimento`,1,2)) >=' => substr($dtini,6,4) . substr($dtini,3,2)  . substr($dtini,0,2)
														  ),
													 array(
														 'CONCAT(substring(`Titulo`.`vencimento`,7,4) , substring(`Titulo`.`vencimento`,4,2) , substring(`Titulo`.`vencimento`,1,2)) <= ' => substr($dtfim,6,4) . substr($dtfim,3,2)  . substr($dtfim,0,2)
														  )
											);
				}
			////////}

			// Executa a consulta
			$options = array(
				'fields' => array('*'),
				'conditions' => $conditions,
				'order' => "codmregiao,substring(`Titulo`.`vencimento`,7,4) DESC , substring(`Titulo`.`vencimento`,4,2) DESC , substring(`Titulo`.`vencimento`,1,2) DESC", 
				'limit' => 50
			);

				$this->paginate = $options;
				$noticias = $this->paginate('Titulo');
				$this->set('titulos', $noticias);
		}
			
	}

}
