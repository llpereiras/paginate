<div class="titulos index">

  <?php $dados = $this->request->data; ?>

	<h2><?php echo __('Totais ( Em Aberto X Quitados )' ); ?></h2>
	
	<div class="paging">
	
	<form name="totais_titulos" method="post" action="totais">
	
	<table  width="100%" >
	<tr>
	
	<?php

	/*************************************************************************************/
		$conditions = "";
		$select_consulta = "";
		$text_consulta = "";
		$modulo = "";
		$tipo = "";
		$user_baixa = "";
		$dt_quitacao = "";
		$dtini = "";
		$dtfim = "";
		
		if ($_SERVER['REQUEST_METHOD'] == "POST"){
			if (isset($dados["tipo"]) != "" ) {
			 $tipo = $dados["tipo"];
			}
			
			if (isset($dados["select_consulta"])){
			$select_consulta= $dados["select_consulta"];
			}
			
			if (isset($dados["text_consulta"])){
			$text_consulta= $dados["text_consulta"];
			}
			
			if (isset($dados["dt_quitacao"])){
			$dt_quitacao = $dados["dt_quitacao"];
			}
					
			if (isset($dados["user_baixa"])){
			$user_baixa = $dados["user_baixa"];
			}

			if (isset($dados["dtini"])){
			$dtini = $dados["dtini"];
			}
			
			if (isset($dados["dtfim"])){
			$dtfim = $dados["dtfim"];
			}
		}
		
		
		if ($_SERVER['REQUEST_METHOD'] == "GET"){
			$urli = explode("/",$_SERVER['REQUEST_URI']);
			///////////////////print_r($urli);
			for ($i=0; $i< count($urli); $i++){
				$urlX = explode(":",$urli[$i] );
				if (count($urlX) > 1 ){
				
					 if ($urlX[0] == "tipo"){ 
						$tipo = str_replace ("_"," ",$urlX[1]);
					}
						
					 if ($urlX[0] == "select_consulta"){ 
						$select_consulta = $urlX[1];
					}
						
					if ($urlX[0] == "text_consulta"){ 
						$text_consulta = $urlX[1];
					}
					
					if ($urlX[0] == "dt_quitacao"){
					$dt_quitacao = $urlX[1];
					}
							
					if ($urlX[0] == "user_baixa"){
					$user_baixa = $urlX[1];
					}

					if ($urlX[0] == "dtini"){
					$dtini = $urlX[1];
					}
					
					if ($urlX[0] == "dtfim"){
					$dtfim = $urlX[1];
					}
					
			 
				}
			}
		}
		/*****************************************************************************************************/
				
	?>
	<td><input type="radio" name="tipo" id="aberto" value="aberto" checked <?php if($tipo == "aberto"){ echo "checked"; } ?> ><label>Em Aberto</label></td>
	<td><input type="radio" name="tipo" id="quitados" value="quitados" <?php if($tipo == "quitados"){ echo "checked"; } ?> ><label>Quitados</label> </td>
	<td><input type="radio" name="tipo" id="todos" value="todos" <?php if($tipo == "todos"){ echo "checked"; } ?> ><label>Todos</label> </td>
	
	</tr>
	
	<tr>
		<td><label>Data Inicial</label> </td>
		<td><input type="text" name="dtini" id="dtini" size="5" class="xxx" value="<?php echo $dtini; ?>" ></td>
		<td><label>Data Final</label> </td>
		<td><input type="text" name="dtfim" id="dtfim" size="5" class="xxx" value="<?php echo $dtfim; ?>" ></td>
	</tr>
	
	<tr>
	<td>Filtro Adicional</td>
	<td colspan="2">
	<select name="select_consulta" id = "select_consulta">
		<option value =""></option>
		<option value ="codmregiao" <?php if($select_consulta == "codmregiao"){ echo " selected "; } ?> >Regiao</option>
		<option value ="cod_cliente" <?php if($select_consulta == "cod_cliente"){ echo " selected "; } ?> >Codigo Cliente</option>
	</select>
	
	</td>
	<td><input type="text"  name="text_consulta" id = "text_consulta" value="<?php echo $text_consulta; ?>" size="1" class="xxx"></td>
	</tr>
	
	
	<tr>
	<td>Respos&aacute;vel pela Baixa</td>
	<td colspan="2">
	<select name="user_baixa" id = "user_baixa">
		<option value =""></option>
		<?php foreach ($users as $user): ?>
			<option value ="<?php echo $user['Users']['username']; ?>" <?php if($user_baixa == $user['Users']['username']){ echo " selected "; } ?> ><?php echo $user['Users']['username']; ?></option>
		<?php endforeach; ?>
	</select>
	
	</td>
	<td colspan="2">Data de quita&ccedil;&atilde;o</td>
	<td><input type="text"  name="dt_quitacao" id = "dt_quitacao" value="<?php echo $dt_quitacao; ?>" size="1" class="xxx"></td>
	</tr>
	
	<tr>
	<td> <input type="submit" value ="Processar">	</td>
	</tr>
	
	<?php
	
		$totaldetitulos = 0 ;
		$abertos = 0; 
		$quitados = 0 ;
		
		// Processa os totais antes de processar a listagem
		foreach ($titulos as $titulo):
		$totaldetitulos = $totaldetitulos + $titulo['Titulo']['valor_inicial'];
		
		// Abertos
		if ($titulo['Titulo']['data_baixa'] =='' || $titulo['Titulo']['data_baixa'] == null){
		    $abertos = $abertos + $titulo['Titulo']['valor_inicial'];
		}
		
		// Quitados
		if ($titulo['Titulo']['data_baixa'] !=''){
		    $quitados = $quitados + $titulo['Titulo']['valor_inicial'];
		}
		

		
		
		
		endforeach; 
		// Fim do processamento dos totais
		
	?>
	</table>
	
	
	<table  cellpadding="0" cellspacing="0"  width="40%" id="tbl_resultados">

	
		
	<tr>
	<td> <label><b>Abertos </b></td><td style="text-align: right;"><?php echo number_format($abertos, 2, '.', ','); ?></td>
	</tr>
		
	<tr>
	<td> <label><b>Quitados </b></td><td style="text-align: right;"><?php echo number_format($quitados, 2, '.', ','); ?></td>
	</tr>
		
	<tr>
	<td> <label><b>Diferen&ccedil;a </b></td><td style="text-align: right;"><?php echo number_format($abertos - $quitados, 2, '.', ','); ?><b></label> </td>
	</tr>
		
	<tr>
	<td><label><b>Valor Total dos Titulos</td>
	<td style="text-align: right;"> <label><b><?php echo number_format(h($totaldetitulos), 2, '.', ','); ?></b></label> </td>
	</tr>
	
	</table>
	</form>
	

	<?php
		
		$this->Paginator->options(array('url' => array('tipo' => $tipo , 'dtini' => $dtini , 'dtfim' => $dtfim , 'select_consulta' => $select_consulta ,'text_consulta' => $text_consulta , 'user_baixa' => $user_baixa , 'dt_quitacao' => $dt_quitacao)));
		echo $this->Paginator->first('Primeira');	
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('proximo') . ' >', array(), null, array('class' => 'next disabled'));
		echo $this->Paginator->last('Ultima');	
	?>
	</div>
	
	
	<table cellpadding="0" cellspacing="0" width="95%">
	
	<tr>
			<!-- <th><?php echo $this->Paginator->sort('nseq'); ?></th> --> 
			<th width="2%"><?php echo $this->Paginator->sort('codmregiao','Regiao'); ?></th>
			
			<!-- <th><?php echo $this->Paginator->sort('cod_representante'); ?></th> -->
			<!-- <th width="20%"><?php echo $this->Paginator->sort('Representante'); ?></th> -->
			
			<!-- <th><?php echo $this->Paginator->sort('cgc','Cnpj'); ?></th> -->
			<th width="3%" ><?php echo $this->Paginator->sort('cod_cliente'); ?></th>
			<th width="50%"><?php echo $this->Paginator->sort('nome'); ?></th>
			
			<th width="8%" ><?php echo $this->Paginator->sort('emissao'); ?></th>
			<th width="8%" ><?php echo $this->Paginator->sort('n_documento','fatura'); ?></th>
			<th width="8%" ><?php echo $this->Paginator->sort('vencimento'); ?></th>
					
			<th width="8%" ><?php echo $this->Paginator->sort('valor_inicial','Valor'); ?></th>
			<!-- <th><?php echo $this->Paginator->sort('valor_inicial1'); ?></th> -->
			<th width="8%" ><?php echo $this->Paginator->sort('val_tit_x_comiss','Comissao'); ?></th>
		
			<!-- <th><?php echo $this->Paginator->sort('comissao_r_cli'); ?></th> -->
			<!-- <th><?php echo $this->Paginator->sort('tipo_pedido'); ?></th> -->
			<!-- <th><?php echo $this->Paginator->sort('comissao_r_cli1'); ?></th> -->
			<!-- <th><?php echo $this->Paginator->sort('descricao'); ?></th> -->
			<!-- <th><?php echo $this->Paginator->sort('cod_tpreco'); ?></th> -->
			 <!-- <th><?php echo $this->Paginator->sort('constant'); ?></th> --> 
			 <th><?php echo $this->Paginator->sort('data_baixa'); ?></th> 
			 <th><?php echo $this->Paginator->sort('user_id_baixa','Baixado Por'); ?></th> 
			<th class="actions"><?php echo "&nbsp;";//__('Ações'); ?></th>
	</tr>
	<?php  foreach ($titulos as $titulo): ?>

	<tr>
		<!--<td><?php echo h($titulo['Titulo']['nseq']); ?>&nbsp;</td> -->
		<td><?php echo h($titulo['Titulo']['codmregiao']); ?>&nbsp;</td>
		<!-- <td><?php echo h($titulo['Titulo']['cod_representante']); ?>&nbsp;</td> -->
		<!-- <td><?php echo h($titulo['Titulo']['nome1']); ?>&nbsp;</td> -->
		
		<td><?php echo h($titulo['Titulo']['cod_cliente']); ?>&nbsp;</td>
		<td ><?php echo h($titulo['Titulo']['nome']); ?>&nbsp;</td>
		
		
		<td><?php echo h($titulo['Titulo']['emissao']); ?>&nbsp;</td>
		<td><?php echo h($titulo['Titulo']['n_documento']); ?>&nbsp;</td>
		<td><?php echo h($titulo['Titulo']['vencimento']); ?>&nbsp;</td>
		<!-- <td><?php echo h($titulo['Titulo']['cgc']); ?>&nbsp;</td> -->
			
		<td style="text-align: right;"><?php echo number_format(h($titulo['Titulo']['valor_inicial']), 2, '.', ','); ?>&nbsp;</td>
		<!-- <td><?php echo h($titulo['Titulo']['valor_inicial1']); ?>&nbsp;</td> -->
		<td style="text-align: right;"><?php echo number_format(h($titulo['Titulo']['val_tit_x_comiss']), 2, '.', ','); ?>&nbsp;</td>
	
		<!-- <td><?php echo h($titulo['Titulo']['comissao_r_cli']); ?>&nbsp;</td> -->
		<!-- <td><?php echo h($titulo['Titulo']['tipo_pedido']); ?>&nbsp;</td> -->
		<!-- <td><?php echo h($titulo['Titulo']['comissao_r_cli1']); ?>&nbsp;</td> -->
		<!-- <td><?php echo h($titulo['Titulo']['descricao']); ?>&nbsp;</td> -->
		<!-- <td><?php echo h($titulo['Titulo']['cod_tpreco']); ?>&nbsp;</td> -->
		<!--<td><?php echo h($titulo['Titulo']['constant']); ?>&nbsp;</td> -->
		
		<td><?php echo h($titulo['Titulo']['data_baixa']); ?>&nbsp;</td>
		<td><?php echo h($titulo['Titulo']['user_id_baixa']); ?>&nbsp;</td>
		
		<td class="actions">
			<!-- <?php echo $this->Html->link(__('Visualizar'), array('action' => 'view', $titulo['Titulo']['nseq'])); ?> --> 
			<?php 
			 //if ($titulo['Titulo']['data_baixa'] == null || $titulo['Titulo']['data_baixa'] == ''){
			 //echo $this->Form->postLink(__('Quitar'), array('action' => 'delete', $titulo['Titulo']['nseq']), null, __('Confirma baixa do titulo %s?', $titulo['Titulo']['nseq'])); 
			 //echo $this->Html->link(__('Editar'), array('action' => 'edit', $titulo['Titulo']['nseq']));
			//}
			?>
			
			
			<!-- <?php echo $this->Form->postLink(__('Deletar'), array('action' => 'delete', $titulo['Titulo']['nseq']), null, __('Deseja excluir o titulo selecionado %s?', $titulo['Titulo']['nseq'])); ?> -->
		</td>
	</tr>
<?php endforeach; ?>

	
	
	
	</table>
	<p>
	<?php 
	echo $this->Paginator->counter(array(
	'format' => __('Pagina {:page} de {:pages}, Mostrando {:current} registro de um total de {:count} , Comecando pelo registro {:start}, terminando no {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php 
		echo $this->Paginator->first('Primeira');	
		echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('proximo') . ' >', array(), null, array('class' => 'next disabled'));
		echo $this->Paginator->last('Ultima');	
		
	?>
	</div>
</div>
<div class="actions">
	<?php
$valor = $this->Session->read('Auth.User');  //Retorna o array com o id, nome do usuário e password.
echo "Bem vindo <b>" . $valor['username'] . "<b>"; //Exibi o nome do usuario.
echo "<br><br>";

?>
	<ul>
		<li><?php echo $this->Html->link(__('Menu'), '../menu'); ?></li>
		<li><?php echo $this->Html->link(__('Novo Titulo'), array('action' => 'add')); ?></li>
		
		
		<li><?php echo $this->Html->link(__('Logout'), '../users/logout'); ?></li>
	</ul>
	<ul>
		
</div>
<?php 

function rdzDt($datax){
$datax =  substr ($datax,0,3) .  substr ($datax,3,3) .  substr ($datax,8,2) ;
return $datax;
}

?>
